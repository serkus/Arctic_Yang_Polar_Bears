import { Redirect, Route } from "react-router-dom";
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs
} from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import {
  bookmarkOutline,
  cafeOutline,
  mapOutline,
  menu,
  walletOutline
} from "ionicons/icons";
import Place from "./pages/Place";
import Education from "./pages/Education";
import Tab3 from "./pages/Tab3";
import Menu from "./pages/Menu";
import Works from "./pages/Works";
import Space from "./pages/Space";

import "@ionic/react/css/core.css";

import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

import "./theme/variables.css";
import { List } from "./pages/List";

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonTabs>
        <IonRouterOutlet>
          <Route exact path="/">
            <Redirect to="/places" />
          </Route>
          <Route exact path="/places">
            <Place />
          </Route>
          <Route exact path="/edu">
            <Education />
          </Route>
          <Route exact path="/space">
            <Space />
          </Route>
          <Route path="/tab3">
            <Tab3 />
          </Route>

          <Route path="/menu3">
            <Menu />
          </Route>
          <Route path="/works">
            <Works />
          </Route>
        </IonRouterOutlet>

        <IonTabBar slot="bottom">
          <IonTabButton tab="places" href="/places">
            <IonIcon icon={mapOutline} />
            <IonLabel>Места</IonLabel>
          </IonTabButton>
          <IonTabButton tab="educ" href="/edu">
            <IonIcon icon={bookmarkOutline} />
            <IonLabel>"Образование</IonLabel>
          </IonTabButton>
          <IonTabButton tab="space" href="/space">
            <IonIcon icon={cafeOutline} />
            <IonLabel>"Пространства</IonLabel>
          </IonTabButton>
          <IonTabButton tab="arbaten" href="/works">
            <IonIcon icon={walletOutline} />
            <IonLabel>Работа</IonLabel>
          </IonTabButton>
          {/* <IonTabButton tab="tab3" href="/tab3">
            <IonIcon icon={pencil} />
            <IonLabel>Профиль</IonLabel>
</IonTabButton>*/}
          <IonTabButton tab="menu" href="/menu">
            <IonIcon icon={menu} />
            <IonLabel>Ещё</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
      <Route path="/list">
        <List />
      </Route>
    </IonReactRouter>
  </IonApp>
);

export default App;
