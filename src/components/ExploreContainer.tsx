import './ExploreContainer.css';

interface ContainerProps {
  name: string;
}

const ExploreContainer: React.FC<ContainerProps> = (props) => {
  return (
    <div className="container">
      <strong>{props.name}</strong>
        <div>{props.children || ''}</div>
    </div>
  );
};

export default ExploreContainer;
