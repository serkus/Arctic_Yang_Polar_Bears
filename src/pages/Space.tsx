import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonSegment,
  IonLabel,
  IonSegmentButton
} from "@ionic/react";
//import { tabletPortraitSharp } from "ionicons/icons";

const Tab2: React.FC = () => {
  const tabApp = [
    { title: "Молодёжные пространства", value: "YangSpaces" },
    { title: "Еда", value: "Food" },
    { title: "Кофейни", value: "Cafe" }
  ];
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Пространства</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense"></IonHeader>
        <IonSegment
          onIonChange={(e) => console.log("Segment selected", e.detail.value)}
          style={{ fontSize: 8, padding: 5 }}
        >
          {tabApp.map((i) => (
            <IonSegmentButton value={i.value} style={{ fontSize: "12px" }}>
              <IonLabel>{i.title}</IonLabel>
            </IonSegmentButton>
          ))}
        </IonSegment>
      </IonContent>
    </IonPage>
  );
};

export default Tab2;
