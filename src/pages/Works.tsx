import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonSegment,
  IonLabel,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardTitle,
  IonIcon,
  IonButton,
  IonSegmentButton
} from "@ionic/react";
import "./Tab2.css";

import { heart } from "ionicons/icons";

const Tab2: React.FC = () => {
  const tablist = [
    {
      title: "Работа с опытом",
      value: "Instituts",
      handler: () => EduView
    },
    {
      title: "Работа без опыта/стажировка",
      value: "Grands",
      handler: () => GrandsView
    }
  ];

  const WorkList = [
    { title: "Монтажник", price: "44000", likes: "28", experience: "" },
    { title: "Ситемный  Инженер", price: "68000", likes: "60", experience: "" },
    { title: "Проограммист", price: "120000", likes: "50", experience: "" },
    { title: "DevOps", price: "86000", likes: "100", experience: "" },
    { title: "", price: "", likes: "", experience: "" },
    { title: "", price: "", likes: "", experience: "" },
    {}
  ];

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Вакансии</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense"></IonHeader>
        <IonSegment
          onIonChange={(e) => console.log("Segment selected", e.detail.value)}
        >
          {tablist.map((i) => (
            <IonSegmentButton value={i.value} style={{ fontSize: "12px" }}>
              <IonLabel>{i.title}</IonLabel>
            </IonSegmentButton>
          ))}
        </IonSegment>
        {WorkList.map((w) => (
          <IonCard>
            <IonCardHeader>
              <IonCardTitle>{w.title}</IonCardTitle>
            </IonCardHeader>
            <p>
              <span style={{ fontSize: "20px", float: "right" }}>
                <b>{w.price} &#8399;</b>
              </span>
              <IonIcon icon={heart} style={{ color: "#F00", fontSize: 16 }} />
              {w.likes}
            </p>
            <IonCardContent>
              {w.experience}

              <p
                style={{
                  padding: "10px",
                  alignItems: "center"
                }}
              >
                <IonButton
                  color="lifght"
                  style={{ backgroundColor: "#EE5203", color: "#EEE" }}
                >
                  Хочу подробностей
                </IonButton>{" "}
              </p>
            </IonCardContent>
          </IonCard>
        ))}
      </IonContent>
    </IonPage>
  );
};

export default Tab2;
