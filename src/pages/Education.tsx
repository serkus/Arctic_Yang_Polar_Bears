import { useState } from "react";
import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonSegment,
  IonLabel,
  IonSegmentButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardTitle,
  IonImg,
  IonButton
} from "@ionic/react";

//import "./Tab2.css";
import EduCard from "../components/CardEdu";

const Tab2: React.FC = () => {
  //const [eduState, setEduState] = useState([
  //]);

  const EduLIst = [
    {
      name: "Мурманский государственный технический университет",
      description: "",
      logo:
        "https://yt3.ggpht.com/ytc/AKedOLReX8Tka9MQXKoYtog_4PFbztOqgZKZO82NyDYoFA=s900-c-k-c0x00ffffff-no-rj "
    },
    {
      name: "Мурманский арктический государственный университет",
      description: "",
      logo:
        "https://sun9-70.userapi.com/s/v1/if1/8I5ksYhMI17W_HHf1F5dKDbT9RIaGXvPRC7S4x3FwbhyA5uuY2sDGqbIoyAPn0hfNOpI7iFY.jpg?size=443x447&quality=96&crop=80,0,443,447&ava=1"
    },
    {
      name: "Мурманский педагогический колледж",
      description: "",
      logo: "https://mpc-murmansk.ru/img/all/186_mpk_logo.jpg"
    },
    {
      /* name: "", description: "", logo: "" },
    { name: "", description: "", logo: "" },
    { name: "", description: "", logo: "" },
    { name: "", description: "", logo: "" } */
    }
  ];

  const EduView = () =>
    EduLIst.map((edu) => (
      <IonCard>
        <IonCardHeader style={{ display: "inline" }}>
          <IonImg
            src={edu.logo}
            style={{
              width: "120px",
              height: "120px",
              float: "left",

              margin: "20px"
            }}
          />
          <IonCardTitle>{edu.name}</IonCardTitle>
        </IonCardHeader>

        <IonCardContent>
          {edu.description}
          <p style={{ textAlign: "center" }}>
            <IonButton
              color="lifght"
              style={{ backgroundColor: "#EE5203", color: "#EEE" }}
            >
              Хочу подробностей
            </IonButton>{" "}
          </p>
        </IonCardContent>
      </IonCard>
    ));
  const GrandsView = () => (
    <>
      <a href="https://мурманск.гранты.рф/?ysclid=l5l3kahe2u203584674"></a>
      <a href="https://rscf.ru/contests/?ysclid=l5l3m8z1ja498967331"></a>
    </>
  );
  const tablist = [
    {
      title: "Институты/Колледжи",
      value: "Instituts",
      handler: EduView
    },
    {
      title: "Грантовая поддержка",
      value: "Grands",
      handler: GrandsView
    }
  ];

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Образование</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense"></IonHeader>
        <IonSegment
          style={{ fontSize: "8px", color: "#00F" }}
          onIonChange={(e) => e.detail.value}
        >
          {tablist.map((i) => (
            <IonSegmentButton value={i.handler} style={{ fontSize: "12px" }}>
              <IonLabel>{i.title}</IonLabel>
            </IonSegmentButton>
          ))}
        </IonSegment>
        <IonContent>{EduView()}</IonContent>
      </IonContent>
    </IonPage>
  );
};

export default Tab2;
