import {
  IonCard,
  IonCardHeader,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonCardTitle,
  IonCardContent,
  IonImg,
  IonIcon,
  IonButton
} from "@ionic/react";

import { navigateOutline } from "ionicons/icons";

import "./Tab1.css";
//import { Link } from "react-router-dom";
const Tab1: React.FC = () => {
  const genKey = () => Math.random().toString();
  const cites = [
    {
      name: "Мурманск",
      description:
        "Город на северо-западе России, административный центр Мурманской области",
      image:
        "https://travelsoul.ru/wp-content/uploads/a/3/4/a34b7266b82357999628736e466e7e77.jpeg",
      distance: "0км, Вы на месте",
      key: genKey()
    },
    {
      name: "Кировск",
      description:
        "Ки́ровск (в 1931-1934 годах — Хибиного́рск) — город в Мурманской области России, у горного массива Кукисвумчорр.Что посетить: поход в горы Хибины/сап летом и горные лыжи/сноуборд в Кировске зимой. Как добраться: Рейсовые автобусы и личный автотранспорт",
      image:
        "https://gulaytour.ru/wp-content/uploads/2018/03/4636210-1024x682.jpg",
      distance:
        "Расстояние от Кировска до Мурманска – 214 километров по трассе",
      key: genKey()
    },
    {
      name: "Териберка",
      description:
        "Тери́берка — село в Кольском районе Мурманской области. Расположено на Мурманском берегу Кольского полуострова в устье одноимённой реки, при впадении её в губу Териберскую Баренцева моря. Что посетить: Левиафан, кладбище кораблей, охота на китов, Северно-ледовитый океан, пляж драконьих яиц, норвежские пейзажи, батарейский водопад. Как добраться: Рейсовые автобусы и личный автотранспорт",
      distance: "Расстояние от Мурманска до Териберки 130 километров",
      image:
        "https://static.tildacdn.com/tild6162-3932-4634-b938-353736373963/156356462917696187.jpg",
      key: genKey()
    },
    {
      name: "Кировск",
      description:
        "Ки́ровск (в 1931-1934 годах — Хибиного́рск) — город в Мурманской области России, у горного массива Кукисвумчорр.Что посетить: поход в горы Хибины/сап летом и горные лыжи/сноуборд в Кировске зимой. Как добраться: Рейсовые автобусы и личный автотранспорт",
      image:
        "https://avatars.mds.yandex.net/get-zen_doc/3429702/pub_624c4eea5b725700b4dce688_624c4f3062661e7fd361f496/scale_1200",
      key: genKey()
    }
  ];

  /*
  const url = "https://api.molodaya-arctica.ru/api/cities"
  const response = fetch(url, {
    'method': 'GET',
    'headers': {
      'Content-Type': "application/json;charset=utf-8",
      'Accept': "application/json",
      'Authorization':
      "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNjA3NDBhZDBkYTI3YjM0NGM5ZDBhNGZiNTNhNzdmNzU5MjY2YzgyZTQ0MzJlNmE3OTE2M2ZkZjRjN2YxNzc0Y2Y0MWY1NTIxNWZkMTIyNTIiLCJpYXQiOjE2NTYzMjA2NjEuNzg3NjgzLCJuYmYiOjE2NTYzMjA2NjEuNzg3Njg0LCJleHAiOjE2ODc4NTY2NjEuNzg0OTQ2LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.h6t8LsDT1DbsRhUUL7tBds3NIezTaZQKRxlbfJ96sTIZf6OWq3hNl8pDGuM7zovIua57ylPufskgu7avX6X4ZBrf2eWE8Y-ch2mAg9LDR8lEYLhs9H9DS4S1JFmQvBU-LQkJfnytlhTgeilJ4Stw0GdSO40TQg-VuDWW5R5tSb18sEfa0kLyM2GlcDYgRRWjJxUsBl9Oxz5UJiKvW_banCFtJS6Bmzuz-GseBKOw7SqHR3PwxwX1QvO5vRMfMMoVJhPIJ5fwNYpDjICxJoAG4dG7h8DZbSSjb_CitcvuXKVmZvdlOxkuBUv0FvkwkuMvhfIGnKWomEypmbBiUniOZBlvrcxzQPkVkzqgiJPgKtS3_8U4KRbdY0Is7PhxluO6PR7eF5V81otTlAO4CmC4VB3Ne1gp9jnE0EbF9Da-c8kjBq1roqkLWIWEgOovOR-56CbSFth6c6QR0MeV6Q_54SFB5ltWw88Y5ZnqHjHg-ePVd4fY61aII3l8SKXwuJDGeFoXX-772Sl3bnk5gLx8kgpwrbBplOS0CkkJdClFoahEzPsXu32WJHjqMy6KACIj7coP5d8EprgeGe266-ZBS6kkgVuFLOqQQhaz5ekXtisiMRvIkiNC3taIYwQH2Vi7vsvr3hr-oTgnMSFnXDgHnwVZNqDk825vu6Yw-5MAlLM'"
    }
  });
  let result = response.json()
*/
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Достопримечательности</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 1</IonTitle>
          </IonToolbar>
        </IonHeader>
        {cites.map((c) => (
          <IonCard>
            <IonCardHeader>
              <IonImg
                src={c.image}
                style={{ width: "100%", height: "150px" }}
              />
              <IonCardTitle>{c.name}</IonCardTitle>
            </IonCardHeader>

            <IonCardContent>
              {c.description}

              <p
                style={{
                  padding: "10px",
                  border: "1px  dotted #AAA",
                  fontSize: "16px"
                }}
              >
                <IonIcon
                  icon={navigateOutline}
                  style={{ padding: "5px", fontSize: "20px" }}
                />
                <b>{c.distance}</b>
              </p>
              <p style={{ textAlign: "center" }}>
                <IonButton
                  color="lifght"
                  style={{ backgroundColor: "#EE5203", color: "#EEE" }}
                >
                  Хочу подробностей
                </IonButton>{" "}
              </p>
            </IonCardContent>
          </IonCard>
        ))}
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
