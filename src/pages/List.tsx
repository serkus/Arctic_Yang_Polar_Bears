import {IonContent, IonHeader, IonIcon, IonLabel, IonPage, IonRouterOutlet, IonTabBar, IonTabButton, IonTabs, IonTitle, IonToolbar} from '@ionic/react';
import {Redirect, Route, useHistory} from 'react-router-dom';
import {ellipse, square, triangle} from "ionicons/icons";
import {ListTab} from "./ListTab";
import {useEffect} from "react";
import {useLocation} from "react-router";

export const List: React.FC = () => {
    /*const location = useLocation();

    useEffect(() => {
        const pathName = location.pathname;
        const tabBar: HTMLElement | null = document.querySelector('.tabBar');
        if(tabBar) {
            setTimeout(() => {
                const selectButtonList: NodeList = tabBar.querySelectorAll(`ion-tab-button.tab-selected`);
                if(selectButtonList && selectButtonList.length){
                    selectButtonList.forEach((selectButton) => {
                        (selectButton as HTMLElement).classList.remove('tab-selected');
                    });
                }
                const button: HTMLElement | null = tabBar.querySelector(`ion-tab-button[href="${pathName}"]`);
                if(button) {
                    button.classList.add('tab-selected');
                }
            }, 10);
        }
    }, [location]);*/

    return (
    <IonPage>
        <IonHeader>
            <IonToolbar>
                <IonTitle>Notification Title</IonTitle>
            </IonToolbar>
        </IonHeader>
        <IonContent>
            <IonTabs>
                <IonRouterOutlet>
                    <Route exact path="/list/tab-1">
                        <ListTab name={'List Tab 1'} />
                    </Route>
                    <Route exact path="/list/tab-2">
                        <ListTab name={'List Tab 2' } />
                    </Route>
                    <Route exact path="/list/tab-3">
                        <ListTab name={'List Tab 3' } />
                    </Route>
                    <Route exact path="/list">
                        <Redirect to="/list/tab-1" />
                    </Route>
                </IonRouterOutlet>
                <IonTabBar slot="top" class={'tabBar'}>
                    <IonTabButton tab="list/tab-1" href="/list/tab-1">
                        <IonIcon icon={triangle} />
                        <IonLabel>ListTab 1</IonLabel>
                    </IonTabButton>
                    <IonTabButton tab="list/tab-2" href="/list/tab-2">
                        <IonIcon icon={ellipse} />
                        <IonLabel>ListTab 2</IonLabel>
                    </IonTabButton>
                    <IonTabButton tab="list/tab-3" href="/list/tab-3">
                        <IonIcon icon={square} />
                        <IonLabel>ListTab 3</IonLabel>
                    </IonTabButton>
                </IonTabBar>
            </IonTabs>
        </IonContent>
    </IonPage>
    );
}
